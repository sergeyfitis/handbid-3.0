package com.handbid.android.presenters;

import com.handbid.android.data.databasemodels.TermsAndConditions;
import com.handbid.android.startup.StartupPresenter;
import com.handbid.android.startup.StartupUseCase;
import com.handbid.android.startup.StartupView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import rx.Subscriber;

import static org.mockito.Mockito.verify;

/**
 * Created by sergeyfitis on 21.03.2016.
 */
public class StartupPresenterTest {

    private static final int TERMS_ID = 444;
    private static final String TEST_TERMS = "Test terms";
    @Mock
    StartupView startupView;
    @Mock
    StartupUseCase startupUseCase;


    private StartupPresenter presenter;
    @Captor
    private ArgumentCaptor<Subscriber<TermsAndConditions>> subscriberArgumentCaptor;

    @Before
    public void setupTest() {
        MockitoAnnotations.initMocks(this);
        presenter = new StartupPresenter(startupUseCase);
        presenter.setView(startupView);
    }

    @Test
    public void testInitializePresenterSuccess() throws Exception {
        presenter.initialize();
        verify(startupView).showLoader();
        verify(startupUseCase).execute(subscriberArgumentCaptor.capture());

        TermsAndConditions termsAndConditions = new TermsAndConditions();
        termsAndConditions.setId(TERMS_ID);
        termsAndConditions.setHtmlText(TEST_TERMS);

        subscriberArgumentCaptor.getValue().onNext(termsAndConditions);
        verify(startupView).hideLoader();
        verify(startupView).updateTermsAndConditions(termsAndConditions.getHtmlText());
    }

    @Test
    public void testInitializePresenterFailure() throws Exception {
        presenter.initialize();
        verify(startupView).showLoader();
        verify(startupUseCase).execute(subscriberArgumentCaptor.capture());

        TermsAndConditions termsAndConditions = new TermsAndConditions();
        termsAndConditions.setId(TERMS_ID);
        termsAndConditions.setHtmlText(TEST_TERMS);

        subscriberArgumentCaptor.getValue().onError(new Throwable());
        verify(startupView).hideLoader();
        verify(startupView).showRetryDialog();
    }
}
