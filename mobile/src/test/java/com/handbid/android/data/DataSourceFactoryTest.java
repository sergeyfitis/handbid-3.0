package com.handbid.android.data;

import com.handbid.android.data.datasource.CloudDataSourceImpl;
import com.handbid.android.data.datasource.DataSource;
import com.handbid.android.data.datasource.DataSourceFactory;
import com.handbid.android.data.datasource.LocalDataSourceImpl;
import com.handbid.android.data.db.DbManager;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

/**
 * Created by sergeyfitis on 21.03.2016.
 */
public class DataSourceFactoryTest {
    @Mock
    DbManager dbManager;

    private CountDownLatch countDownLatch;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        countDownLatch = new CountDownLatch(1);
    }

    @Test
    public void cloudDataSource_shouldReturnCorrect() throws InterruptedException{
        DataSource dataSource = DataSourceFactory.create(dbManager);
        assertTrue(dataSource instanceof CloudDataSourceImpl);
        countDownLatch.await(5, TimeUnit.SECONDS);
    }

    @Test
    public void localDataSource_shouldReturnCorrect() throws InterruptedException {
        DataSource dataSource = DataSourceFactory.create(dbManager);
        assertTrue(dataSource instanceof LocalDataSourceImpl);
        countDownLatch.countDown();
    }

    @Test
    public void cloudDataSourceUpdate_shouldReturnCorrect() throws InterruptedException {
        countDownLatch.await(16, TimeUnit.SECONDS);
        DataSource dataSource = DataSourceFactory.create(dbManager);
        assertTrue(dataSource instanceof CloudDataSourceImpl);
        countDownLatch.countDown();
    }
}
