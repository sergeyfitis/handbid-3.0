package com.handbid.android.data;

import com.handbid.android.data.databasemodels.TermsAndConditions;
import com.handbid.android.data.databasemodels.User;
import com.handbid.android.data.networkmodels.TermsAndConditionsResponse;
import com.handbid.android.data.networkmodels.UserResponse;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Created by sergeyfitis on 16.03.2016.
 */
public class DataConverterTest {
    private TermsAndConditionsResponse dummyResponse;
    
    @Before
    public void setup() {
        dummyResponse = new TermsAndConditionsResponse();
        TermsAndConditionsResponse.Data data = new TermsAndConditionsResponse.Data();
        data.setHtml("Fake Text");
        dummyResponse.setData(data);
    }
    
    @After
    public void finish() {
        dummyResponse = null;
    }
    
    @Test
    public void convertTermsAndConditionsValidity_test() {
        TermsAndConditions termsAndConditions = DataConverter.toTermsAndConditions(dummyResponse);
        Assert.assertNotNull(termsAndConditions);
        Assert.assertEquals(termsAndConditions.getHtmlText(), dummyResponse.getData().getHtml());
        Assert.assertEquals(termsAndConditions.getId(), 1);
    }

    @Test
    public void convertTermsAndConditionsReturnNull_test() {
        TermsAndConditions termsAndConditions = DataConverter.toTermsAndConditions(null);
        assertNull(termsAndConditions);
    }

    @Test
    public void userConverter_testNonNull() throws Exception {
        UserResponse userResponse = new UserResponse();
        userResponse.setId(111);
        userResponse.setName("Walter White");
        userResponse.setCountryCode("UA");
        User user = DataConverter.toUser(userResponse);
        assertEquals(user.getId(), userResponse.getId());
        assertEquals(user.getName(), userResponse.getName());
        assertEquals(user.getCountryCode(), userResponse.getCountryCode());

    }

    @Test
    public void userConverter_testNull() throws Exception {
        assertNull(DataConverter.toUser(null));
    }
}
