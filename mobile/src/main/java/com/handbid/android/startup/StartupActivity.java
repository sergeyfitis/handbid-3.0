package com.handbid.android.startup;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import com.handbid.android.R;
import com.handbid.android.screens.StartupNavigation;
import com.handbid.android.screens.activities.BaseActivity;
import com.handbid.android.signin.SignInFragment;
import com.handbid.android.signup.SignUpFragment;

public class StartupActivity extends BaseActivity implements StartupNavigation {

    private SignUpFragment signUpFragment;
    private SignInFragment signInFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startup);
        final FragmentManager supportFragmentManager = getSupportFragmentManager();
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .add(R.id.fl_startup_container, StartupFragment.newInstance(), StartupFragment.TAG)
                    .commit();
        } else {
            signInFragment = (SignInFragment) supportFragmentManager.getFragment(savedInstanceState, SignInFragment.TAG);
            signUpFragment = (SignUpFragment) supportFragmentManager.getFragment(savedInstanceState, SignUpFragment.TAG);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        final FragmentManager supportFragmentManager = getSupportFragmentManager();
        if (signUpFragment != null) {
            supportFragmentManager.putFragment(outState, SignUpFragment.TAG, signUpFragment);
        }
        if (signInFragment != null) {
            supportFragmentManager.putFragment(outState, SignInFragment.TAG, signInFragment);
        }
    }

    @Override
    public void showSignIn() {
        if (signInFragment == null) {
            signInFragment = SignInFragment.newInstance();
        }
        showFragment(R.id.fl_startup_container, signInFragment, SignInFragment.TAG);
    }

    @Override
    public void showSignUp() {
        if (signUpFragment == null) {
            signUpFragment = SignUpFragment.newInstance();
        }
        showFragment(R.id.fl_startup_container, signUpFragment, SignUpFragment.TAG);
    }

    @Override
    public void showPinCode() {
        // TODO: 04.05.2016 Implementation needed
    }

    @Override
    public void showMain() {
        // TODO: 04.05.2016 Implementation needed
    }
}
