package com.handbid.android.startup;


import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v7.widget.CardView;
import android.support.v7.widget.SwitchCompat;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewFlipper;
import butterknife.*;
import com.handbid.android.R;
import com.handbid.android.screens.StartupNavigation;
import com.handbid.android.screens.activities.TermsActivity;
import com.handbid.android.screens.fragments.BaseFragment;
import com.handbid.android.usecases.UseCasesFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class StartupFragment extends BaseFragment implements StartupView {
    public static final String TAG = "StartupFragment";
    @Bind(R.id.iv_startup_logo)
    ImageView ivStartupLogo;
    @Bind(R.id.vf_startup_hints)
    ViewFlipper vfStartupHints;
    @Bind(R.id.cv_terms_and_conditions)
    CardView cvTermsAndConditions;
    @Bind(R.id.tv_terms_and_conditions)
    TextView tvTermsAndConditions;
    @Bind(R.id.sc_agree_terms_and_conditions)
    SwitchCompat scTermsAnsConditions;
    @Bind(R.id.fab_startup_next)
    FloatingActionButton fabNext;

    @BindInt(android.R.integer.config_longAnimTime)
    int animationDuration;

    @BindColor(R.color.colorAccent)
    int fabEnabledColor;
    @BindColor(R.color.colorAccentTransparent)
    int fabDisabledColor;

    private String termsAndConditions;

    private boolean needOpenTerms = false;
    private boolean isTermsLoading = false;

    private final FastOutSlowInInterpolator interpolator = new FastOutSlowInInterpolator();

    private StartupPresenter presenter;
    @Nullable
    private StartupNavigation startupNavigation;


    public static StartupFragment newInstance() {
        StartupFragment startupFragment = new StartupFragment();
        startupFragment.setRetainInstance(true);
        return startupFragment;
    }


    public StartupFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new StartupPresenter(UseCasesFactory.newStartupUseCase());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_startup, container, false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.attachView(this);
        updateUI();
        view.getViewTreeObserver()
                .addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                    @Override
                    public boolean onPreDraw() {
                        view.getViewTreeObserver().removeOnPreDrawListener(this);
                        startupAnimation();
                        return true;
                    }
                });
        presenter.initialize();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.detachView();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        startupNavigation = (StartupNavigation) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        startupNavigation = null;
    }

    @OnClick(R.id.tv_terms_and_conditions)
    void showTerms() {
        if (isTermsLoading) {
            needOpenTerms = true;
        } else if (!TextUtils.isEmpty(termsAndConditions)) {
            Intent intent = new Intent(getContext(), TermsActivity.class);
            intent.putExtra(TermsActivity.KEY_TERMS_AND_CONDITIONS, termsAndConditions);
            ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeCustomAnimation(getContext(),
                    R.anim.slide_in_from_bottom, R.anim.zoom_out);
            ActivityCompat.startActivity(getActivity(), intent, optionsCompat.toBundle());
        } else {
            showRetryDialog();
        }
    }

    @OnCheckedChanged(R.id.sc_agree_terms_and_conditions)
    void onCheckedChange(boolean checked) {
        fabNext.setEnabled(checked);
    }

    @OnClick(R.id.fab_startup_next)
    void onNext() {
        if (startupNavigation != null) {
            startupNavigation.showSignIn();
        }
    }

    private void updateUI() {

        String terms1 = getString(R.string.terms_and_conditions_title);
        String terms2 = getString(R.string.user_terms_and_conditions);
        String allTitle = terms1 + " " + terms2;
        int startHighLightIndex = allTitle.indexOf(terms2);
        int endHighLightIndex = allTitle.length();
        SpannableString spannableString = new SpannableString(allTitle);
        spannableString.setSpan(new StyleSpan(Typeface.BOLD), startHighLightIndex, endHighLightIndex, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        spannableString.setSpan(new UnderlineSpan(), startHighLightIndex, endHighLightIndex, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark)), startHighLightIndex, endHighLightIndex, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        tvTermsAndConditions.setText(spannableString);
    }


    private void startupAnimation() {
        ViewCompat.setAlpha(ivStartupLogo, 0f);
        ViewCompat.setAlpha(vfStartupHints, 0f);
        ViewCompat.setTranslationY(ivStartupLogo, ivStartupLogo.getHeight() / -2f);
        ViewCompat.setTranslationY(vfStartupHints, vfStartupHints.getHeight() / -2f);
        ViewCompat.setTranslationY(cvTermsAndConditions, cvTermsAndConditions.getHeight() * 2f);


        ViewCompat.animate(ivStartupLogo)
                .setDuration(animationDuration)
                .alpha(1f)
                .setInterpolator(interpolator)
                .translationY(0f)
                .start();

        ViewCompat.animate(vfStartupHints)
                .setDuration(animationDuration)
                .alpha(1f)
                .setInterpolator(interpolator)
                .translationY(0f)
                .withEndAction(() -> {
                    vfStartupHints.setFlipInterval(5000);
                    vfStartupHints.setAutoStart(true);
                })
                .start();


        ViewCompat.animate(cvTermsAndConditions)
                .setDuration(animationDuration)
                .alpha(1f)
                .setInterpolator(new OvershootInterpolator())
                .translationY(0f)
                .start();
    }

    @Override
    public void updateTermsAndConditions(String termsAndConditions) {
        this.termsAndConditions = termsAndConditions;
        if (needOpenTerms) {
            needOpenTerms = false;
            showTerms();
        }
    }

    @Override
    public void showLoader() {
        isTermsLoading = true;
    }

    @Override
    public void hideLoader() {
        isTermsLoading = false;
    }

    @Override
    public void showRetryDialog() {

    }

    @Override
    public boolean hasInternetConnection() {
        return true;
    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showNoInternetConnectionMessage() {

    }
}
