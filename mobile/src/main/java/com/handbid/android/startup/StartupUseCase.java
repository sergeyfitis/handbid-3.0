package com.handbid.android.startup;

import com.handbid.android.data.databasemodels.TermsAndConditions;
import com.handbid.android.data.datasource.DataSource;
import com.handbid.android.executor.ExecutionThread;
import com.handbid.android.executor.ThreadExecutor;
import com.handbid.android.usecases.UseCase;

import rx.Observable;

/**
 * Created by sergeyfitis on 21.03.2016.
 */
public class StartupUseCase extends UseCase<TermsAndConditions> {

    private DataSource dataSource;

    public StartupUseCase(ThreadExecutor threadExecutor, ExecutionThread postExecutionThread, DataSource dataSource) {
        super(threadExecutor, postExecutionThread);
        this.dataSource = dataSource;
    }

    @Override
    protected Observable<TermsAndConditions> buildUseCaseObservable() {
        return dataSource.termsAndConditions();
    }
}
