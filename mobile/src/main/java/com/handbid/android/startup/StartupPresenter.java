package com.handbid.android.startup;

import com.handbid.android.data.databasemodels.TermsAndConditions;
import com.handbid.android.presenters.BasePresenter;
import com.handbid.android.usecases.DefaultSubscriber;

/**
 * Created by sergeyfitis on 21.03.2016.
 */
public class StartupPresenter extends BasePresenter<StartupView> {

    private StartupUseCase startupUseCase;


    public StartupPresenter(StartupUseCase startupUseCase) {
        this.startupUseCase = startupUseCase;
    }

    public void initialize() {
        getView().showLoader();
        startupUseCase.execute(new StartupSubscription());
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {
        getView().hideLoader();
    }

    @Override
    public void onDestroy() {
        startupUseCase.unsubscribe();
    }

    private class StartupSubscription extends DefaultSubscriber<TermsAndConditions> {
        @Override
        public void onNext(TermsAndConditions termsAndConditions) {
            getView().hideLoader();
            getView().updateTermsAndConditions(termsAndConditions.getHtmlText());
        }

        @Override
        public void onError(Throwable e) {
            getView().hideLoader();
            getView().showRetryDialog();
        }
    }

}
