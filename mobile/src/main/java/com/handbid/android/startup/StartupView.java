package com.handbid.android.startup;

import com.handbid.android.views.BaseView;

/**
 * Created by sergeyfitis on 18.03.2016.
 */
public interface StartupView extends BaseView {

    void  updateTermsAndConditions(String termsAndConditions);

}
