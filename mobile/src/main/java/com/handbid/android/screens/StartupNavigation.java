package com.handbid.android.screens;

/**
 * Created by serge on 04.05.2016.
 */
public interface StartupNavigation {
    void showSignIn();

    void showSignUp();

    void showPinCode();

    void showMain();
}
