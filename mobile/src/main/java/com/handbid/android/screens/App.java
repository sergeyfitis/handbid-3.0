package com.handbid.android.screens;

import android.app.Application;

import com.handbid.android.data.preferences.Prefs;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by sergeyfitis on 16.03.2016.
 */
public class App extends Application {
    public static final String DB_FILE_NAME = "handbid.realm";
    public static final int DB_SCHEME_VERSION = 1;

    @Override
    public void onCreate() {
        super.onCreate();
        RealmConfiguration configuration = new RealmConfiguration.Builder(this)
                .deleteRealmIfMigrationNeeded()
                .name(DB_FILE_NAME)
                .schemaVersion(DB_SCHEME_VERSION)
                .build();
        Realm.setDefaultConfiguration(configuration);
        Prefs.init(this);
    }
}
