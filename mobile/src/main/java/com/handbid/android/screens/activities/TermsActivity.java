package com.handbid.android.screens.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.util.Linkify;
import android.widget.TextView;

import com.handbid.android.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class TermsActivity extends AppCompatActivity {

    public static final String KEY_TERMS_AND_CONDITIONS = "key_terms_and_conditions";

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.tv_terms_and_conditions_text)
    TextView tvTermsAndConditionsText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        if (getIntent() != null) {
            String terms = getIntent().getStringExtra(KEY_TERMS_AND_CONDITIONS);
            tvTermsAndConditionsText.setAutoLinkMask(Linkify.WEB_URLS);
            tvTermsAndConditionsText.setText(Html.fromHtml(terms));
        } else {
            finish();
        }
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.terms_and_conditions_activity);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        overridePendingTransition(R.anim.zoom_in, R.anim.slide_out_to_bottom);
    }

    @Nullable
    @Override
    public Intent getSupportParentActivityIntent() {
        return getIntent();
    }
}
