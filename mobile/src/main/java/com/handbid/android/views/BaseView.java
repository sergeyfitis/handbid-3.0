package com.handbid.android.views;

import android.content.Context;

/**
 * Created by sergeyfitis on 17.03.2016.
 */
public interface BaseView {
    void showLoader();
    void hideLoader();
    void showRetryDialog();
    boolean hasInternetConnection();
    void showMessage(String message);
    void showNoInternetConnectionMessage();
    Context getContext();
}
