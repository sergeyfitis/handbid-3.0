package com.handbid.android.signup;

import com.handbid.android.data.databasemodels.User;
import com.handbid.android.data.networkmodels.LoginResponse;
import com.handbid.android.data.networkmodels.RegisterResponse;
import com.handbid.android.views.BaseView;

/**
 * Created by sergeyfitis on 24.03.2016.
 */
public interface SignUpView extends BaseView {

    void onUserRegistered(RegisterResponse registerResponse);

    void onUserLoggedIn(LoginResponse loginResponse);

    void onUserLoaded(User user);

}
