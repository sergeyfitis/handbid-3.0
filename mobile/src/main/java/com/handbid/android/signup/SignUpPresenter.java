package com.handbid.android.signup;

import com.handbid.android.data.exception.ErrorMessageFactory;
import com.handbid.android.data.networkmodels.LoginResponse;
import com.handbid.android.data.networkmodels.RegisterResponse;
import com.handbid.android.presenters.BasePresenter;
import com.handbid.android.usecases.DefaultSubscriber;
import com.handbid.android.usecases.UseCase;

/**
 * Created by sergeyfitis on 24.03.2016.
 */
public class SignUpPresenter extends BasePresenter<SignUpView> {

    private final UseCase<RegisterResponse> registerUseCase;
    private final UseCase<LoginResponse> loginUseCase;

    public SignUpPresenter(UseCase<RegisterResponse> registerUseCase, UseCase<LoginResponse> loginUseCase) {
        this.registerUseCase = registerUseCase;
        this.loginUseCase = loginUseCase;
    }

    public void registerUser() {
        if (getView().hasInternetConnection()) {
            registerUseCase.execute(new RegisterSubscriber());
        } else {
            getView().showNoInternetConnectionMessage();
        }
    }

    public void loginUser() {
        if (getView().hasInternetConnection()) {
            loginUseCase.execute(new LoginSubscriber());
        } else {
            getView().showNoInternetConnectionMessage();
        }
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onDestroy() {
        registerUseCase.unsubscribe();
    }

    private class RegisterSubscriber extends DefaultSubscriber<RegisterResponse> {
        @Override
        public void onCompleted() {
            super.onCompleted();
            getView().hideLoader();
        }

        @Override
        public void onError(Throwable e) {
            super.onError(e);
            getView().hideLoader();
            getView().showMessage(ErrorMessageFactory.create(getView().getContext(), (Exception) e));
        }

        @Override
        public void onNext(RegisterResponse registerResponse) {
            super.onNext(registerResponse);
            getView().onUserRegistered(registerResponse);
            if (registerResponse.isSuccess()) {
                loginUseCase.execute(new LoginSubscriber());
            } else {
                getView().showMessage(ErrorMessageFactory.create(getView().getContext(), null));
            }
        }
    }


    private class LoginSubscriber extends DefaultSubscriber<LoginResponse> {
        @Override
        public void onCompleted() {
            super.onCompleted();
            getView().hideLoader();
        }

        @Override
        public void onError(Throwable e) {
            super.onError(e);
            getView().hideLoader();
            getView().showMessage(ErrorMessageFactory.create(getView().getContext(), (Exception) e));
        }

        @Override
        public void onNext(LoginResponse loginResponse) {
            super.onNext(loginResponse);
            getView().hideLoader();
            if (loginResponse.isSuccess()) {
                getView().onUserLoggedIn(loginResponse);
            } else {
                getView().showMessage(ErrorMessageFactory.create(getView().getContext(), null));
            }
        }
    }
}