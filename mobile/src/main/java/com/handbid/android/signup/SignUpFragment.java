package com.handbid.android.signup;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.Bind;
import com.handbid.android.R;
import com.handbid.android.data.databasemodels.User;
import com.handbid.android.data.networkmodels.LoginResponse;
import com.handbid.android.data.networkmodels.RegisterResponse;
import com.handbid.android.helpers.StringUtils;
import com.handbid.android.screens.fragments.BaseFragment;
import com.handbid.android.usecases.UseCasesFactory;

import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignUpFragment extends BaseFragment implements SignUpView {
    public static final String TAG = "SignUpFragment";

    public static final String KEY_SIGN_UP_PARAMS = "sign_up_params";
    public static final String KEY_SIGN_IN_PARAMS = "sign_in_params";

    @Bind(R.id.til_sign_up_f_name)
    TextInputLayout tilFirstName;
    @Bind(R.id.til_sign_up_l_name)
    TextInputLayout tilLastName;
    @Bind(R.id.et_user_first_name)
    EditText etFirstName;
    @Bind(R.id.et_user_last_name)
    EditText etLastName;
    @Bind(R.id.til_sign_up_email)
    TextInputLayout tilEmail;
    @Bind(R.id.et_user_email)
    EditText etEmail;
    @Bind(R.id.tv_country_code)
    TextView tvCountryCode;
    @Bind(R.id.til_sign_up_phone)
    TextInputLayout tilPhone;
    @Bind(R.id.et_user_phone)
    EditText etPhone;

    private SignUpPresenter presenter;

    private final HashMap<String, String> signUpParams = new HashMap<>();
    private final HashMap<String, String> signInParams = new HashMap<>();


    public static SignUpFragment newInstance() {
        return new SignUpFragment();
    }

    public SignUpFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new SignUpPresenter(UseCasesFactory.newSignUpUseCase(signUpParams),
                UseCasesFactory.newSignInUseCase(signInParams));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sign_up, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.attachView(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.detachView();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(KEY_SIGN_IN_PARAMS, new HashMap<>(signInParams));
        outState.putSerializable(KEY_SIGN_UP_PARAMS, new HashMap<>(signUpParams));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(KEY_SIGN_IN_PARAMS)) {
                signInParams.clear();
                //noinspection unchecked
                signInParams.putAll((HashMap<String, String>) savedInstanceState.getSerializable(KEY_SIGN_IN_PARAMS));
            }
            if (savedInstanceState.containsKey(KEY_SIGN_UP_PARAMS)) {
                signUpParams.clear();
                //noinspection unchecked
                signUpParams.putAll((HashMap<String, String>) savedInstanceState.getSerializable(KEY_SIGN_UP_PARAMS));
            }
        }
    }

    public void doSignUp() {
        if (validateParams()) {
            presenter.registerUser();
        }
    }

    @Override
    public void onUserRegistered(RegisterResponse registerResponse) {

    }

    @Override
    public void onUserLoggedIn(LoginResponse loginResponse) {

    }

    @Override
    public void onUserLoaded(User user) {

    }

    @Override
    public void showLoader() {
        Toast.makeText(getContext(), "Doing something :)", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void hideLoader() {

    }

    @Override
    public void showRetryDialog() {

    }

    @Override
    public boolean hasInternetConnection() {
        return true;
    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showNoInternetConnectionMessage() {

    }

    private String getCountryCode() {
        return (StringUtils.isEmpty(tvCountryCode) ? "" : tvCountryCode.getText().toString()) +
                (StringUtils.isEmpty(etPhone) ? "" : etPhone.getText());
    }

    private boolean validateParams() {
        boolean isEmailValid = !StringUtils.isEmpty(etEmail) &&
                Patterns.EMAIL_ADDRESS.matcher(etEmail.getText()).matches();

        boolean isFirstNameValid = !StringUtils.isEmpty(etFirstName);
        boolean isLastNameValid = !StringUtils.isEmpty(etLastName);

        final String phoneNumber = getCountryCode();
        boolean isPhoneNumberValid = !StringUtils.isEmpty(phoneNumber) &&
                Patterns.PHONE.matcher(phoneNumber).matches();

        tilFirstName.setError(isFirstNameValid ? null : getString(R.string.err_invalid_first_name));
        tilLastName.setError(isLastNameValid ? null : getString(R.string.err_invalid_last_name));
        tilEmail.setError(isEmailValid ? null : getString(R.string.err_invalid_email));
        tilPhone.setError(isPhoneNumberValid ? null : getString(R.string.err_invalid_phone_number));

        return isFirstNameValid && isLastNameValid &&
                isEmailValid && isPhoneNumberValid;
    }
}
