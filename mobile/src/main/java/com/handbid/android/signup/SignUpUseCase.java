package com.handbid.android.signup;

import com.handbid.android.data.datasource.DataSource;
import com.handbid.android.data.networkmodels.RegisterResponse;
import com.handbid.android.executor.ExecutionThread;
import com.handbid.android.executor.ThreadExecutor;
import com.handbid.android.usecases.UseCase;

import java.util.Map;

import rx.Observable;

/**
 * Created by sergeyfitis on 24.03.2016.
 */
public class SignUpUseCase extends UseCase<RegisterResponse> {

    private final DataSource dataSource;
    private final Map<String, String> params;

    public SignUpUseCase(ThreadExecutor threadExecutor, ExecutionThread postExecutionThread, DataSource dataSource, Map<String, String> params) {
        super(threadExecutor, postExecutionThread);
        this.dataSource = dataSource;
        this.params = params;
    }

    @Override
    protected Observable<RegisterResponse> buildUseCaseObservable() {
        return dataSource.registerUser(params);
    }
}
