package com.handbid.android.presenters;

import com.handbid.android.views.BaseView;

/**
 * Created by sergeyfitis on 27.04.2016.
 */
public abstract class BasePresenter<V extends BaseView> implements MvpPresenter<V> {

    protected V view;

    @Override
    public void attachView(V view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    public V getView() {
        return view;
    }
}
