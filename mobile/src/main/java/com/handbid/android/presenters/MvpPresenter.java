package com.handbid.android.presenters;

import com.handbid.android.views.BaseView;

/**
 * Created by sergeyfitis on 21.03.2016.
 */
public interface MvpPresenter<T extends BaseView> {
    void onResume();
    void onPause();
    void onDestroy();
    void attachView(T view);
    void detachView();
}
