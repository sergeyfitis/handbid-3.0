package com.handbid.android.helpers;

import android.support.annotation.Nullable;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

/**
 * Created by sergeyfitis on 11.04.2016.
 */
public class StringUtils {

    public static boolean isEmpty(@Nullable CharSequence sequence) {
        return sequence == null || sequence.length() == 0;
    }

    public static boolean isEmpty(@Nullable EditText editText) {
        return editText == null || isEmpty(editText.getText());
    }

    public static boolean isEmpty(@Nullable TextView textView) {
        return textView == null || isEmpty(textView.getText());
    }

    public static ArrayList<String> isoCountries() {
        String[] isoCountries = Locale.getISOCountries();
        ArrayList<String> countries = new ArrayList<>(isoCountries.length);
        for (int i = 0; i < isoCountries.length; i++) {
            Locale locale = new Locale(Locale.ENGLISH.getLanguage(), isoCountries[i]);
            countries.add(locale.getDisplayCountry());
        }
        Collections.sort(countries);
        return countries;
    }

}
