package com.handbid.android.data.restapi;

import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by sergeyfitis on 16.03.2016.
 */
public class HandbidApiManager {
    private static HandbidApiService sInstance;

    private HandbidApiManager() {
    }

    public static HandbidApiService getInstance() {
        if (sInstance == null) {
            final Interceptor requestsInterceptor = chain -> {
                Request request = chain.request().newBuilder()
                        .header(C.HEADER_AUTHORIZATION, "ADD TOKEN")
                        .header(C.HEADER_ACCEPT, C.ACCEPT_JSON)
                        .header(C.HEADER_CONTENT_TYPE, C.ACCEPT_JSON)
                        .header(C.HEADER_COPYRIGHT, C.COPYRIGHT_VALUE)
                        .header(C.HEADER_PACKAGE, C.PACKAGE_VALUE)
                        .header(C.HEADER_LICENCE, C.LICENSE_VALUE)
                        .header(C.HEADER_LINK, C.LINK_VALUE)
                        .header(C.HEADER_AUTHOR, C.AUTHOR_VALUE)
                        .build();
                return chain.proceed(request);
            };
            final HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .addInterceptor(requestsInterceptor)
                    .addInterceptor(loggingInterceptor)
                    .readTimeout(10, TimeUnit.SECONDS)
                    .writeTimeout(10, TimeUnit.SECONDS)
                    .retryOnConnectionFailure(true)
                    .build();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(C.BASE_URL)
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
            sInstance = retrofit.create(HandbidApiService.class);
        }

        return sInstance;
    }

}
