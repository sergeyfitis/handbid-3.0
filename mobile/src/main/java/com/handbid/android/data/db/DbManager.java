package com.handbid.android.data.db;

import com.handbid.android.data.databasemodels.TermsAndConditions;
import com.handbid.android.data.databasemodels.User;

import rx.Observable;

/**
 * Created by sergeyfitis on 16.03.2016.
 */
public interface DbManager {

    Observable<TermsAndConditions> saveTermsAndConditionsSync(TermsAndConditions termsAndConditions);

    Observable<TermsAndConditions> getTermsAndConditions();

    Observable<User> loadUser(int userId);

    Observable<User> saveUser(User user);

}
