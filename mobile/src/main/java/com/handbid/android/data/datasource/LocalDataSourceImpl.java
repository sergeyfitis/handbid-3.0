package com.handbid.android.data.datasource;

import com.handbid.android.data.databasemodels.TermsAndConditions;
import com.handbid.android.data.databasemodels.User;
import com.handbid.android.data.db.DbManager;
import com.handbid.android.data.networkmodels.LoginResponse;
import com.handbid.android.data.networkmodels.RegisterResponse;
import com.handbid.android.data.preferences.Prefs;

import java.util.Map;

import rx.Observable;

/**
 * Created by sergeyfitis on 16.03.2016.
 */
public class LocalDataSourceImpl implements DataSource {

    private final DbManager dbManager;

    public LocalDataSourceImpl(DbManager dbManager) {
        this.dbManager = dbManager;
    }

    @Override
    public Observable<TermsAndConditions> termsAndConditions() {
        return dbManager.getTermsAndConditions();
    }

    @Override
    public Observable<RegisterResponse> registerUser(Map<String, String> params) {
        throw new UnsupportedOperationException("Do not save this");
    }

    @Override
    public Observable<LoginResponse> loginUser(Map<String, String> params) {
        throw new UnsupportedOperationException("Do not save this");
    }

    @Override
    public Observable<User> loadUser() {
        return dbManager.loadUser(Prefs.getUserId());
    }
}
