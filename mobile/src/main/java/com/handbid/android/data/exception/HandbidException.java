package com.handbid.android.data.exception;

/**
 * Created by sergeyfitis on 22.03.2016.
 */
public class HandbidException extends Exception {
    public HandbidException() {
        super();
    }

    public HandbidException(String detailMessage) {
        super(detailMessage);
    }

    public HandbidException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public HandbidException(Throwable throwable) {
        super(throwable);
    }
}
