package com.handbid.android.data.datasource;

import com.handbid.android.data.databasemodels.TermsAndConditions;
import com.handbid.android.data.databasemodels.User;
import com.handbid.android.data.networkmodels.LoginResponse;
import com.handbid.android.data.networkmodels.RegisterResponse;

import java.util.Map;

import rx.Observable;

/**
 * Created by sergeyfitis on 16.03.2016.
 */
public interface DataSource {

    Observable<TermsAndConditions> termsAndConditions();

    Observable<RegisterResponse> registerUser(Map<String, String> params);

    Observable<LoginResponse> loginUser(Map<String, String> params);

    Observable<User> loadUser();
}
