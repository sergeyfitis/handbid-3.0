package com.handbid.android.data.db;

import com.handbid.android.data.databasemodels.TermsAndConditions;
import com.handbid.android.data.databasemodels.User;

import io.realm.Realm;
import rx.Observable;

/**
 * Created by sergeyfitis on 16.03.2016.
 */
public class DbManagerImpl implements DbManager {
    @Override
    public Observable<TermsAndConditions> saveTermsAndConditionsSync(TermsAndConditions termsAndConditions) {
        return Observable.fromCallable(() -> {
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(termsAndConditions);
            realm.commitTransaction();
            return termsAndConditions;
        });
    }

    @Override
    public Observable<TermsAndConditions> getTermsAndConditions() {
        // TODO: 16.03.2016 Implement
        return null;
    }

    @Override
    public Observable<User> loadUser(int userId) {
        return Observable.fromCallable(() -> {
            Realm realm = Realm.getDefaultInstance();
            User user = realm
                    .where(User.class)
                    .equalTo(DbFields._ID, userId)
                    .findFirst();
            User fromRealm = realm.copyFromRealm(user);
            realm.close();
            return fromRealm;
        });
    }

    @Override
    public Observable<User> saveUser(User user) {
        return Observable.fromCallable(() -> {
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(user);
            realm.commitTransaction();
            return user;
        });
    }
}
