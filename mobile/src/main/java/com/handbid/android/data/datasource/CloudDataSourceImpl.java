package com.handbid.android.data.datasource;

import com.handbid.android.data.DataConverter;
import com.handbid.android.data.databasemodels.TermsAndConditions;
import com.handbid.android.data.databasemodels.User;
import com.handbid.android.data.db.DbManager;
import com.handbid.android.data.networkmodels.LoginResponse;
import com.handbid.android.data.networkmodels.RegisterResponse;
import com.handbid.android.data.preferences.Prefs;
import com.handbid.android.data.restapi.HandbidApiService;

import java.util.Map;

import rx.Observable;
import rx.functions.Action1;

/**
 * Created by sergeyfitis on 16.03.2016.
 */
public class CloudDataSourceImpl implements DataSource {
    private final HandbidApiService handbidApiService;
    private final DbManager dbManager;

    public CloudDataSourceImpl(HandbidApiService handbidApiService, DbManager dbManager) {
        this.handbidApiService = handbidApiService;
        this.dbManager = dbManager;
    }

    private final Action1<LoginResponse> saveLoginToken = loginResponse -> {
        if (loginResponse.isSuccess()) {
            Prefs.setAuthToken(loginResponse.getData().getToken());
            Prefs.setUserId(loginResponse.getData().getIdentity());
        }
    };

    @Override
    public Observable<TermsAndConditions> termsAndConditions() {
        return Observable.merge(dbManager.getTermsAndConditions(), handbidApiService.termsAndConditions()
                .map(DataConverter::toTermsAndConditions)
                .flatMap(dbManager::saveTermsAndConditionsSync));
    }

    @Override
    public Observable<RegisterResponse> registerUser(Map<String, String> params) {
        return handbidApiService.registerUser(params);
    }

    @Override
    public Observable<LoginResponse> loginUser(Map<String, String> params) {
        return handbidApiService.loginUser(params)
                .doOnNext(saveLoginToken);
    }

    @Override
    public Observable<User> loadUser() {
        return Observable.merge(dbManager.loadUser(Prefs.getUserId()), handbidApiService.loadUser()
                .map(DataConverter::toUser)
                .flatMap(dbManager::saveUser));
    }
}
