package com.handbid.android.data.databasemodels;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by sergeyfitis on 24.03.2016.
 */
public class CreditCard extends RealmObject {

    @PrimaryKey
    private int id;
    private String creditCardsGuid;
    private String lastFour;
    private String cardType;
    private String nameOnCard;
    private int expMonth;
    private int expYear;
    private String isActiveCard;
    private String creditCardHandle;
    private String stripeId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCreditCardsGuid() {
        return creditCardsGuid;
    }

    public void setCreditCardsGuid(String creditCardsGuid) {
        this.creditCardsGuid = creditCardsGuid;
    }

    public String getLastFour() {
        return lastFour;
    }

    public void setLastFour(String lastFour) {
        this.lastFour = lastFour;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getNameOnCard() {
        return nameOnCard;
    }

    public void setNameOnCard(String nameOnCard) {
        this.nameOnCard = nameOnCard;
    }

    public int getExpMonth() {
        return expMonth;
    }

    public void setExpMonth(int expMonth) {
        this.expMonth = expMonth;
    }

    public int getExpYear() {
        return expYear;
    }

    public void setExpYear(int expYear) {
        this.expYear = expYear;
    }

    public String getIsActiveCard() {
        return isActiveCard;
    }

    public void setIsActiveCard(String isActiveCard) {
        this.isActiveCard = isActiveCard;
    }

    public String getCreditCardHandle() {
        return creditCardHandle;
    }

    public void setCreditCardHandle(String creditCardHandle) {
        this.creditCardHandle = creditCardHandle;
    }

    public String getStripeId() {
        return stripeId;
    }

    public void setStripeId(String stripeId) {
        this.stripeId = stripeId;
    }
}
