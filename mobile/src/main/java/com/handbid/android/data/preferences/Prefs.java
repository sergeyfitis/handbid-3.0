package com.handbid.android.data.preferences;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by sergeyfitis on 16.03.2016.
 */
public class Prefs {

    private static final String KEY_TERMS_AND_CONDITIONS_SYNC = "key_terms_date_sync";
    private static final String KEY_USER_AUTH_TOKEN = "key_auth_token";
    private static final String KEY_CURRENT_USER_ID = "key_current_user_id";


    private static SharedPreferences sPrefs;

    private Prefs() {
    }

    public static void init(Application application) {
        if (sPrefs == null) {
            sPrefs = application.getSharedPreferences("handbid_settings", Context.MODE_PRIVATE);
        }
    }

    public static void setTermsSyncDate(long dateInMillis) {
        updatePrefs(KEY_TERMS_AND_CONDITIONS_SYNC, dateInMillis);
    }

    public static long getTermsSyncDate() {
        return sPrefs.getLong(KEY_TERMS_AND_CONDITIONS_SYNC, 0L);
    }

    public static void setAuthToken(String token) {
        updatePrefs(KEY_USER_AUTH_TOKEN, token);
    }

    public static String getAuthToken() {
        return sPrefs.getString(KEY_USER_AUTH_TOKEN, "");
    }

    public static void setUserId(int id) {
        updatePrefs(KEY_CURRENT_USER_ID, id);
    }

    public static int getUserId() {
        return sPrefs.getInt(KEY_CURRENT_USER_ID, 0);
    }

    private static void updatePrefs(String key, long value) {
        sPrefs.edit()
                .putLong(key, value)
                .apply();
    }

    private static void updatePrefs(String key, String value) {
        sPrefs.edit()
                .putString(key, value)
                .apply();
    }

    private static void updatePrefs(String key, int value) {
        sPrefs.edit()
                .putInt(key, value)
                .apply();
    }
}
