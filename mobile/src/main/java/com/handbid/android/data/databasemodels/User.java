package com.handbid.android.data.databasemodels;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by sergeyfitis on 24.03.2016.
 */
public class User extends RealmObject {
    @PrimaryKey
    private int id;
    private String name;
    private String pin;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String userCellPhone;
    private String stripeId;
    private String currentPaddleNumber;
    private String usersGuid;
    private String alias;
    private RealmList<CreditCard> creditCards;
    private boolean hasCreditCard;
    private String currentPlacement;
    private String placementLabel;
    private String countryCode;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUserCellPhone() {
        return userCellPhone;
    }

    public void setUserCellPhone(String userCellPhone) {
        this.userCellPhone = userCellPhone;
    }

    public String getStripeId() {
        return stripeId;
    }

    public void setStripeId(String stripeId) {
        this.stripeId = stripeId;
    }

    public String getCurrentPaddleNumber() {
        return currentPaddleNumber;
    }

    public void setCurrentPaddleNumber(String currentPaddleNumber) {
        this.currentPaddleNumber = currentPaddleNumber;
    }

    public String getUsersGuid() {
        return usersGuid;
    }

    public void setUsersGuid(String usersGuid) {
        this.usersGuid = usersGuid;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public RealmList<CreditCard> getCreditCards() {
        return creditCards;
    }

    public void setCreditCards(RealmList<CreditCard> creditCards) {
        this.creditCards = creditCards;
    }

    public boolean isHasCreditCard() {
        return hasCreditCard;
    }

    public void setHasCreditCard(boolean hasCreditCard) {
        this.hasCreditCard = hasCreditCard;
    }

    public String getCurrentPlacement() {
        return currentPlacement;
    }

    public void setCurrentPlacement(String currentPlacement) {
        this.currentPlacement = currentPlacement;
    }

    public String getPlacementLabel() {
        return placementLabel;
    }

    public void setPlacementLabel(String placementLabel) {
        this.placementLabel = placementLabel;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
}
