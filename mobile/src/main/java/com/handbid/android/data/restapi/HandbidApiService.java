package com.handbid.android.data.restapi;

import com.handbid.android.data.networkmodels.LoginResponse;
import com.handbid.android.data.networkmodels.RegisterResponse;
import com.handbid.android.data.networkmodels.TermsAndConditionsResponse;
import com.handbid.android.data.networkmodels.UserResponse;

import java.util.Map;

import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by sergeyfitis on 16.03.2016.
 */
public interface HandbidApiService {

    @GET(C.TERMS)
    Observable<TermsAndConditionsResponse> termsAndConditions();

    @FormUrlEncoded
    @POST(C.REGISTER)
    Observable<RegisterResponse> registerUser(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST(C.LOGIN)
    Observable<LoginResponse> loginUser(@FieldMap Map<String, String> params);

    @GET(C.USER_PROFILE)
    Observable<UserResponse> loadUser();

}
