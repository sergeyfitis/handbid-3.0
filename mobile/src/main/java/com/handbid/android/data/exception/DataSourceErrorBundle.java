package com.handbid.android.data.exception;

/**
 * Created by sergeyfitis on 22.03.2016.
 */
public class DataSourceErrorBundle implements ErrorBundle {

    private static final String DEFAULT_ERROR_MESSAGE = "Unknown error";

    private final Exception exception;

    public DataSourceErrorBundle(Exception exception) {
        this.exception = exception;
    }

    @Override
    public Exception getException() {
        return exception;
    }

    @Override
    public String getMessage() {
        return exception != null ? exception.getMessage() : DEFAULT_ERROR_MESSAGE;
    }
}
