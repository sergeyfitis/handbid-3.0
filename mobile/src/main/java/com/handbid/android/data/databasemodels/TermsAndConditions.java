package com.handbid.android.data.databasemodels;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by serge on 15.03.2016.
 */
public class TermsAndConditions extends RealmObject {

    @PrimaryKey
    private int id;
    private String htmlText;
    public String getHtmlText() {
        return htmlText;
    }

    public void setHtmlText(String htmlText) {
        this.htmlText = htmlText;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
