package com.handbid.android.data.datasource;

import com.handbid.android.data.db.DbManager;
import com.handbid.android.data.restapi.HandbidApiManager;

import java.util.concurrent.TimeUnit;

/**
 * Created by sergeyfitis on 21.03.2016.
 */
public class DataSourceFactory {

    private static long lastTimeSync = 0L;
    private static final long TIME_SYNC_INTERVAL = TimeUnit.SECONDS.toMillis(20);

    private DataSourceFactory() {
        // You shall  not pass
    }

    public static DataSource create(DbManager dbManager) {
        boolean isDataOutdated = System.currentTimeMillis() - lastTimeSync >= TIME_SYNC_INTERVAL;
        DataSource dataSource;
        if (isDataOutdated) {
            dataSource = createCloudDataSource(dbManager);
            lastTimeSync = System.currentTimeMillis();
        } else {
            dataSource = createLocalDataSource(dbManager);
        }
        return dataSource;
    }

    public static DataSource createCloudDataSource(DbManager dbManager) {
        return new CloudDataSourceImpl(HandbidApiManager.getInstance(), dbManager);
    }

    public static DataSource createLocalDataSource(DbManager dbManager) {
        return new LocalDataSourceImpl(dbManager);
    }
}
