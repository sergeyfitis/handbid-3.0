package com.handbid.android.data.exception;

/**
 * Created by sergeyfitis on 22.03.2016.
 */
public interface ErrorBundle {

    Exception getException();

    String getMessage();
}
