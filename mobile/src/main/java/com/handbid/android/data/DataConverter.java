package com.handbid.android.data;

import com.handbid.android.data.databasemodels.CreditCard;
import com.handbid.android.data.databasemodels.TermsAndConditions;
import com.handbid.android.data.databasemodels.User;
import com.handbid.android.data.networkmodels.TermsAndConditionsResponse;
import com.handbid.android.data.networkmodels.UserResponse;
import com.handbid.android.data.restapi.C;

import java.util.HashMap;
import java.util.Map;

import io.realm.RealmList;

/**
 * Created by sergeyfitis on 16.03.2016.
 */
public class DataConverter {

    public static TermsAndConditions toTermsAndConditions(TermsAndConditionsResponse response) {
        if (response == null) return null;
        TermsAndConditions termsAndConditions = new TermsAndConditions();
        termsAndConditions.setHtmlText(response.getData().getHtml());
        termsAndConditions.setId(1);
        return termsAndConditions;
    }

    public static Map<String, String> toRegisterParams(String fName, String lName, String mobile, String email, String countryCode) {
        HashMap<String, String> map = new HashMap<>();
        map.put(C.REGISTER_FIRST_NAME, fName);
        map.put(C.REGISTER_LAST_NAME, lName);
        map.put(C.REGISTER_EMAIL, C.REGISTER_EMAIL);
        map.put(C.REGISTER_USER_MOBILE_NUMBER, mobile);
        map.put(C.REGISTER_USER_COUNTRY_CODE, countryCode);
        return map;
    }

    public static User toUser(UserResponse userResponse) {
        if (userResponse == null) return null;
        User user = new User();
        user.setId(userResponse.getId());
        user.setAlias(userResponse.getAlias());
        user.setCountryCode(userResponse.getCountryCode());
        user.setCurrentPaddleNumber(userResponse.getCurrentPaddleNumber());
        user.setCurrentPlacement(userResponse.getCurrentPlacement());
        user.setEmail(userResponse.getEmail());
        user.setFirstName(userResponse.getFirstName());
        user.setLastName(userResponse.getLastName());
        user.setName(userResponse.getName());
        user.setHasCreditCard(userResponse.isHasCreditCard());
        user.setPin(userResponse.getPin());
        user.setStripeId(userResponse.getStripeId());
        user.setUserCellPhone(userResponse.getUserCellPhone());
        user.setPhone(userResponse.getPhone());
        user.setPlacementLabel(userResponse.getPlacementLabel());
        user.setUsersGuid(userResponse.getUsersGuid());
        if (userResponse.isHasCreditCard()) {
            RealmList<CreditCard> creditCards = new RealmList<>();
            for (CreditCard creditCard : userResponse.getCreditCards()) {
                creditCards.add(creditCard);
            }
            user.setCreditCards(creditCards);
        }
        return user;
    }

}
