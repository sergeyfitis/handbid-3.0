package com.handbid.android.data.restapi;

/**
 * Created by sergeyfitis on 16.03.2016.
 */
public final class C {

    public static final String PROD_BASE_URL = "https://rest.hand.bid/";
    public static final String DEV_BASE_URL = "https://qi-rest.hand.bid/";
    public static String BASE_URL = PROD_BASE_URL;

    // Headers
    public static final String NAME = "name";
    public static final String EMAIL = "email";
    public static final String PHONE = "phone";
    public static final String ORG_NAME = "orgName";
    public static final String COMMENTS = "comments";
    public static final String COPYRIGHT_VALUE = "Copyright (c) 2014-2015 Handbid Inc.";
    public static final String CATEGORY_VALUE = "handbid";
    public static final String PACKAGE_VALUE = "Handbid2-Android";
    public static final String LICENSE_VALUE = "Proprietary";
    public static final String LINK_VALUE = "http://www.handbid.com/";
    public static final String AUTHOR_VALUE = "Master of Code (worldclass@masterofcode.com)";
    public static final String HEADER_COPYRIGHT = "copyright";
    public static final String HEADER_CATEGORY = "category";
    public static final String HEADER_PACKAGE = "package";
    public static final String HEADER_LICENCE = "license";
    public static final String HEADER_LINK = "link";
    public static final String HEADER_AUTHOR = "author";
    public static final String HEADER_AUTHORIZATION = "Authorization";
    public static final String BEARER_VALUE = "Bearer ";
    public static final String HEADER_ACCEPT = "Accept";
    public static final String ACCEPT_JSON = "application/json";
    public static final String HEADER_CONTENT_TYPE = "Content-Type";

    // Terms and conditions
    public static final String TERMS = "handbid/terms";

    // Register
    public static final String REGISTER = "auth/register";

    public static final String REGISTER_FIRST_NAME = "firstname";
    public static final String REGISTER_LAST_NAME = "lastname";
    public static final String REGISTER_USER_MOBILE_NUMBER = "mobile";
    public static final String REGISTER_USER_COUNTRY_CODE = "countryCode";
    public static final String REGISTER_EMAIL = "email";

    // Login
    public static final String LOGIN = "auth/login";

    public static final String USER_NAME = "username";
    public static final String USER_PASSWORD = "password";

    // User profile
    public static final String USER_PROFILE = "bidder/index";



}
