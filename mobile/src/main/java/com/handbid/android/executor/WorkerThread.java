package com.handbid.android.executor;

import rx.Scheduler;
import rx.schedulers.Schedulers;

/**
 * Created by serge on 15.03.2016.
 */
public class WorkerThread implements ExecutionThread {

    @Override
    public Scheduler getScheduler() {
        return Schedulers.io();
    }
}
