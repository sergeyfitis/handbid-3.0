package com.handbid.android.executor;

import rx.Scheduler;

/**
 * Created by serge on 15.03.2016.
 */
public interface ExecutionThread {

    Scheduler getScheduler();

}
