package com.handbid.android.usecases;

import com.handbid.android.executor.ExecutionThread;
import com.handbid.android.executor.ThreadExecutor;


import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.schedulers.Schedulers;
import rx.subscriptions.Subscriptions;

/**
 * Created by serge on 15.03.2016.
 */
public abstract class UseCase<T> {

    private final ThreadExecutor threadExecutor;
    private final ExecutionThread thread;

    private Subscription subscription = Subscriptions.empty();

    public UseCase(ThreadExecutor threadExecutor, ExecutionThread postExecutionThread) {
        this.threadExecutor = threadExecutor;
        this.thread = postExecutionThread;
    }

    protected abstract Observable<T> buildUseCaseObservable();

    public void execute(Subscriber<T> subscriber) {
        subscription = buildUseCaseObservable()
                .subscribeOn(Schedulers.from(threadExecutor))
                .observeOn(thread.getScheduler())
                .subscribe(subscriber);
    }

    public void unsubscribe() {
        if (!subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }
}
