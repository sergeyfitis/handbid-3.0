package com.handbid.android.usecases;

import com.handbid.android.data.datasource.DataSourceFactory;
import com.handbid.android.data.db.DbManagerImpl;
import com.handbid.android.executor.JobExecutor;
import com.handbid.android.executor.UiThread;
import com.handbid.android.signin.SignInUseCase;
import com.handbid.android.signup.SignUpUseCase;
import com.handbid.android.startup.StartupUseCase;

import java.util.Map;

/**
 * Created by sergeyfitis on 21.03.2016.
 */
public final class UseCasesFactory {

    private static final JobExecutor JOB_EXECUTOR = new JobExecutor();

    private UseCasesFactory() {
        // You shall not pass
    }

    public static StartupUseCase newStartupUseCase() {
        return new StartupUseCase(JOB_EXECUTOR, new UiThread(), DataSourceFactory.create(new DbManagerImpl()));
    }

    public static SignUpUseCase newSignUpUseCase(Map<String, String> params) {
        return new SignUpUseCase(
                JOB_EXECUTOR, new UiThread(), DataSourceFactory.createCloudDataSource(new DbManagerImpl()),
                params);
    }

    public static SignInUseCase newSignInUseCase(Map<String, String> params) {
        return new SignInUseCase(
                JOB_EXECUTOR, new UiThread(), DataSourceFactory.createCloudDataSource(new DbManagerImpl()),
                params);
    }
}
