package com.handbid.android.usecases;

import rx.Subscriber;

/**
 * Created by sergeyfitis on 21.03.2016.
 */
public class DefaultSubscriber<T> extends Subscriber<T> {

    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {

    }

    @Override
    public void onNext(T t) {

    }
}
