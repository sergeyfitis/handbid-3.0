package com.handbid.android.signin;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.handbid.android.R;
import com.handbid.android.data.networkmodels.LoginResponse;
import com.handbid.android.data.restapi.C;
import com.handbid.android.helpers.StringUtils;
import com.handbid.android.screens.StartupNavigation;
import com.handbid.android.screens.fragments.BaseFragment;
import com.handbid.android.signup.SignUpFragment;
import com.handbid.android.usecases.UseCasesFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignInFragment extends BaseFragment implements SignInView {
    public static final String TAG = "SignInFragment";

    @Bind(R.id.btn_google_sign_in)
    Button btnGoogleSignIn;
    @Bind(R.id.til_phone)
    TextInputLayout tilPhone;
    @Bind(R.id.til_email)
    TextInputLayout tilEmail;
    @Bind(R.id.et_phone_number)
    EditText etUserPhone;
    @Bind(R.id.et_user_email)
    EditText etEmail;
    @Bind(R.id.btn_login)
    Button btnLogin;
    @Bind(R.id.tv_country_code)
    TextView tvCountryCode;

    private SignInPresenter signInPresenter;
    @Nullable
    private StartupNavigation startupNavigation;

    private final HashMap<String, String> signInParams = new HashMap<>();
    private final HashMap<String, String> fakeSignUpParams = new HashMap<>();

    public static SignInFragment newInstance() {
        return new SignInFragment();
    }

    public SignInFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        signInPresenter = new SignInPresenter(UseCasesFactory.newSignInUseCase(signInParams),
                UseCasesFactory.newSignUpUseCase(fakeSignUpParams));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sign_in, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        signInPresenter.attachView(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        signInPresenter.detachView();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        startupNavigation = (StartupNavigation) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        startupNavigation = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (!signInParams.isEmpty()) {
            outState.putSerializable(SignUpFragment.KEY_SIGN_IN_PARAMS, new HashMap<>(signInParams));
        }
        if (!fakeSignUpParams.isEmpty()) {
            outState.putSerializable(SignUpFragment.KEY_SIGN_UP_PARAMS, new HashMap<>(fakeSignUpParams));
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(SignUpFragment.KEY_SIGN_IN_PARAMS)) {
                signInParams.clear();
                //noinspection unchecked
                signInParams.putAll((HashMap<String, String>) savedInstanceState.getSerializable(SignUpFragment.KEY_SIGN_IN_PARAMS));
            }
            if (savedInstanceState.containsKey(SignUpFragment.KEY_SIGN_UP_PARAMS)) {
                fakeSignUpParams.clear();
                //noinspection unchecked
                fakeSignUpParams.putAll((HashMap<String, String>) savedInstanceState.getSerializable(SignUpFragment.KEY_SIGN_UP_PARAMS));
            }
        }
    }

    @OnClick(R.id.btn_login)
    void login() {
        signInParams.clear();
        fakeSignUpParams.clear();
        signInParams.putAll(getSingInParams());
        if (!signInParams.isEmpty()) {
            signInPresenter.logIn();
        }
    }

    @OnClick(R.id.tv_country_code)
    void pickCountryCode() {
        ArrayList<String> countries = StringUtils.isoCountries();
    }

    @Override
    public Map<String, String> getSingInParams() {
        Map<String, String> params = new HashMap<>();
        if (isValidEnteredParams()) {
            params.put(C.EMAIL, etEmail.getText().toString());
            params.put(C.PHONE, etUserPhone.getText().toString());
            fakeSignUpParams.put(C.REGISTER_FIRST_NAME, ".");
            fakeSignUpParams.put(C.REGISTER_LAST_NAME, ".");
            fakeSignUpParams.put(C.REGISTER_EMAIL, etEmail.getText().toString());
            fakeSignUpParams.put(C.REGISTER_USER_MOBILE_NUMBER, etUserPhone.getText().toString());
        }
        return params;
    }

    @Override
    public boolean isValidEnteredParams() {

        boolean isPhoneValid = !StringUtils.isEmpty(etUserPhone) && Patterns.PHONE.matcher(etUserPhone.getText()).matches();
        boolean isEmailValid = !StringUtils.isEmpty(etEmail) && Patterns.EMAIL_ADDRESS.matcher(etEmail.getText()).matches();

        tilPhone.setError(isPhoneValid ? null : getString(R.string.invalid_phone_number));
        tilEmail.setError(isEmailValid ? null : getString(R.string.invali_email));

        return isPhoneValid && isEmailValid;
    }

    @Override
    public void onUserLoggedIn(LoginResponse response) {
        if (startupNavigation != null) {
            startupNavigation.showMain();
        }
    }

    @Override
    public void showLoader() {
        Toast.makeText(getContext(), "Doing something :)", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void hideLoader() {

    }

    @Override
    public void showRetryDialog() {

    }

    @Override
    public boolean hasInternetConnection() {
        return true;
    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showNoInternetConnectionMessage() {

    }
}
