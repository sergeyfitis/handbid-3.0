package com.handbid.android.signin;

import com.handbid.android.data.exception.ErrorMessageFactory;
import com.handbid.android.data.networkmodels.LoginResponse;
import com.handbid.android.data.networkmodels.RegisterResponse;
import com.handbid.android.data.restapi.C;
import com.handbid.android.presenters.BasePresenter;
import com.handbid.android.signup.SignUpUseCase;
import com.handbid.android.usecases.DefaultSubscriber;

/**
 * Created by serge on 04.05.2016.
 */
class SignInPresenter extends BasePresenter<SignInView> {
    private final SignInUseCase signInUseCase;
    private final SignUpUseCase signUpUseCase;


    SignInPresenter(SignInUseCase signInUseCase, SignUpUseCase signUpUseCase) {
        this.signInUseCase = signInUseCase;
        this.signUpUseCase = signUpUseCase;
    }

    void logIn() {
        if (getView().hasInternetConnection()) {
            signUpUseCase.execute(new RegisterSubscriber());
        } else {
            getView().showNoInternetConnectionMessage();
        }
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onDestroy() {
        detachView();
    }

    private class LoginSubscriber extends DefaultSubscriber<LoginResponse> {
        @Override
        public void onCompleted() {
        }

        @Override
        public void onError(Throwable e) {
            super.onError(e);
            getView().hideLoader();
            getView().showMessage(ErrorMessageFactory.create(getView().getContext(), (Exception) e));
        }

        @Override
        public void onNext(LoginResponse loginResponse) {
            super.onNext(loginResponse);
            if (loginResponse.isSuccess()) {
                getView().onUserLoggedIn(loginResponse);
            } else {
                getView().hideLoader();
                getView().showMessage(ErrorMessageFactory.create(getView().getContext(), null));
            }
        }
    }

    private class RegisterSubscriber extends DefaultSubscriber<RegisterResponse> {
        @Override
        public void onCompleted() {
            super.onCompleted();
            getView().hideLoader();
        }

        @Override
        public void onError(Throwable e) {
            super.onError(e);
            getView().hideLoader();
            getView().showMessage(ErrorMessageFactory.create(getView().getContext(), (Exception) e));
        }

        @Override
        public void onNext(RegisterResponse registerResponse) {
            super.onNext(registerResponse);
            if (registerResponse.isSuccess()) {
                signInUseCase.updateParams(C.USER_PASSWORD, registerResponse.getData().getPin());
                signInUseCase.updateParams(C.USER_NAME, registerResponse.getData().getUsername());
                signInUseCase.execute(new LoginSubscriber());
            } else {
                getView().showMessage(ErrorMessageFactory.create(getView().getContext(), null));
            }
        }
    }
}
