package com.handbid.android.signin;

import com.handbid.android.data.datasource.DataSource;
import com.handbid.android.data.networkmodels.LoginResponse;
import com.handbid.android.executor.ExecutionThread;
import com.handbid.android.executor.ThreadExecutor;
import com.handbid.android.usecases.UseCase;
import rx.Observable;

import java.util.Map;

/**
 * Created by sergeyfitis on 24.03.2016.
 */
public class SignInUseCase extends UseCase<LoginResponse> {

    private final DataSource dataSource;
    private final Map<String, String> params;

    public SignInUseCase(ThreadExecutor threadExecutor, ExecutionThread postExecutionThread, DataSource dataSource, Map<String, String> params) {
        super(threadExecutor, postExecutionThread);
        this.dataSource = dataSource;
        this.params = params;
    }

    public void updateParams(String key, String value) {
        params.put(key, value);
    }

    @Override
    protected Observable<LoginResponse> buildUseCaseObservable() {
        return dataSource.loginUser(params);
    }
}
