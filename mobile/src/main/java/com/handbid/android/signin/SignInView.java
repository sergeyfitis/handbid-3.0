package com.handbid.android.signin;

import com.handbid.android.data.networkmodels.LoginResponse;
import com.handbid.android.views.BaseView;

import java.util.Map;

/**
 * Created by sergeyfitis on 11.04.2016.
 */
public interface SignInView extends BaseView {
    Map<String, String> getSingInParams();
    boolean isValidEnteredParams();

    void onUserLoggedIn(LoginResponse response);
}
